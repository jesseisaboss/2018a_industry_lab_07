package ictgradschool.industry.lab07.ex05;

import ictgradschool.Keyboard;

/**
 * TODO Write a program according to the Exercise Five guidelines in your lab handout.
 */
public class ExerciseFive {

    public void start() {
        // TODO Write the codes :)
        try {
            String input1 = exception();
            printsFirstLetters(input1);
        } catch (ExceedMaxStringLengthException e) {
            System.out.println(e);
        } catch (InvalidwordException e) {
            System.out.println(e);

        }
    }

    private String exception() throws ExceedMaxStringLengthException, InvalidwordException {
        System.out.println("please type a string less than 100 characters.");
        String input1 = Keyboard.readInput();
        if (input1.length() > 100) {
            throw new ExceedMaxStringLengthException();

        } else if (input1.charAt(0) >= '0' && input1.charAt(0) <= '9' && input1.charAt(1) >= '0' && input1.charAt(1) <= '9') {
            throw new InvalidwordException();
        }
        return input1;
    }

    // TODO Write some methods to help you.

    public void printsFirstLetters(String input1){
        // below creates an array to split all the places with spaces into an array
        String [] words = input1.split(" ");
        // this loop below iterates through all the words which the words[i] does which becomes a string
        // that can now have.charAt (0) applied to. it then prints this.
        for (int i = 0; i< words.length; i++){
            System.out.println(words[i].charAt(0));
        }


    }




    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        new ExerciseFive().start();


    }
}
