package ictgradschool.industry.lab07.ex05;

public class InvalidwordException extends Exception{
    public InvalidwordException(){}
    public String toString(){
        return "That is an invalid word";
    }
}
